import pygame

def draw_game_layout(screen, mouse_key_list):
    for pos_x in range(0, 300, 100):
                for pos_y in range(0, 300, 100):
                    pygame.draw.rect(screen, (192, 192, 192),
                                    pygame.Rect(pos_x, pos_y, 100, 100))
                    for mouse_coordinate in mouse_key_list:
                        mx, my, image = mouse_coordinate
                        if pos_x <= mx and pos_x + 100 >= mx and pos_y <= my and pos_y + 100 >= my:
                            screen.blit(image, (pos_x, pos_y))
                            if image:
                                break
                    pygame.draw.rect(screen, (0, 0, 0),
                                    pygame.Rect(pos_x, pos_y, 10, 100))
                    pygame.draw.rect(screen, (0, 0, 0),
                                    pygame.Rect(pos_x, pos_y, 100, 10))

