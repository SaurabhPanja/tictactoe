import pygame

pygame.font.init()
myfont = pygame.font.SysFont('calibri', 14)

def draw_game_scorecard(screen, player1_score, player2_score):
    # red_color = (255, 160, 122)
    # blue_color = (65, 105, 225)
    green_color = (50, 205, 50)
    # player1_text = myfont.render(
        # f'''Player X : {player2_score}''', False, (0, 0, 0))
    # player2_text = myfont.render(
        # f'''Player O : {player1_score}''', False, (0, 0, 0))
    reset_text1 = myfont.render("Press 'r' to reset", False, (0, 0, 0))
    # reset_text2 = myfont.render("to reset", False, (0, 0, 0))
    # pygame.draw.rect(screen, red_color, pygame.Rect(10, 310, 100, 50))
    # pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(100, 310, 10, 100))
    # pygame.draw.rect(screen, blue_color, pygame.Rect(110, 310, 100, 50))
    # pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(200, 310, 10, 100))
    pygame.draw.rect(screen, green_color, pygame.Rect(10, 310, 300, 50))
    # screen.blit(player1_text, (20, 320))
    # screen.blit(player2_text, (120, 320))
    screen.blit(reset_text1, (110, 320))
    # screen.blit(reset_text2, (240, 332))
    pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(300, 310, 10, 100))
    pygame.display.flip()