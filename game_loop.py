import pygame
from screen import init_screen
from draw_game_layout import draw_game_layout
from draw_game_scorecard import draw_game_scorecard
from winning_logic import winning_logic
from messagebox import message_box

pygame.init()

def game_loop(screen):
    mouse_key_list = []

    cross_image = pygame.image.load(r'C:\Users\SaurabhPanja\Documents\mountBlue\pygame_tictactoe\cross.png')
    cross_image = pygame.transform.scale(cross_image, (100, 100))

    circle_image = pygame.image.load(r'C:\Users\SaurabhPanja\Documents\mountBlue\pygame_tictactoe\circle.jpg')
    circle_image = pygame.transform.scale(circle_image, (100, 100))
    done = False
    player1_score = 0
    player2_score = 0
    can_click = True
    count = 0
    # b0, b1, b2, b3, b4, b5, b6, b7, b8 = 0, 0, 0, 0, 0, 0, 0, 0, 0
    b = [0]*9
    while not done:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
            if event.type == pygame.MOUSEBUTTONDOWN and can_click:
                if count % 2 == 0:
                    image = cross_image
                else:
                    image = circle_image
                mx, my = pygame.mouse.get_pos()
                mouse_key_list.append((mx, my, image))

                # b0, b1, b2, b3, b4, b5, b6, b7, b8, count, mouse_key_list, can_click, player1_score, player2_score = winning_logic(mx, my, b0, b1, b2, b3, b4, b5, b6, b7, b8, count, mouse_key_list, can_click, player1_score, player2_score)
                # winning_logic(mx, my, b0, b1, b2, b3, b4, b5, b6, b7, b8, count, mouse_key_list, can_click, player1_score, player2_score)

                b, count, mouse_key_list, can_click, player1_score, player2_score = winning_logic(mx, my, b, count, mouse_key_list, can_click, player1_score, player2_score)

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_r:
                    # b0, b1, b2, b3, b4, b5, b6, b7, b8 = 0, 0, 0, 0, 0, 0, 0, 0, 0
                    b = [0]*9
                    mouse_key_list.clear()
                    can_click = True
                    count = 0

        draw_game_layout(screen, mouse_key_list)
        draw_game_scorecard(screen, player1_score, player2_score)
        
        if count == 9 and player1_score == 0 and player2_score == 0:
            message_box("Draw", "It is draw")
            count = 0
        
        if player1_score > 0:
            message_box("Winner", "Player O Won")
            player1_score = 0
            count = 0
        
        if player2_score > 0:
            message_box("Winner", "Player X Won")
            player2_score = 0
            count = 0



